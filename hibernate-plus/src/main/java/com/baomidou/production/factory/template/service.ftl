package com.app.master.core.service;

import IService;
import com.app.master.vo.V${domainName};

/**
 * ${description}
 * @time	${date}
 */
public interface ${domainName?cap_first}Service extends IService<V${domainName}> {


}

